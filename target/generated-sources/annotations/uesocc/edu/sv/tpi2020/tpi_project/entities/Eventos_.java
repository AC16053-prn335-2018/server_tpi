package uesocc.edu.sv.tpi2020.tpi_project.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import uesocc.edu.sv.tpi2020.tpi_project.entities.Calendario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-07T12:33:56")
@StaticMetamodel(Eventos.class)
public class Eventos_ { 

    public static volatile SingularAttribute<Eventos, Integer> hora;
    public static volatile SingularAttribute<Eventos, Integer> idEvento;
    public static volatile SingularAttribute<Eventos, Integer> segundos;
    public static volatile SingularAttribute<Eventos, Integer> duracion;
    public static volatile SingularAttribute<Eventos, Integer> mes;
    public static volatile SingularAttribute<Eventos, Integer> minutos;
    public static volatile SingularAttribute<Eventos, String> nombre;
    public static volatile SingularAttribute<Eventos, Integer> dia;
    public static volatile SingularAttribute<Eventos, Integer> anio;
    public static volatile SingularAttribute<Eventos, Calendario> idCalendario;

}