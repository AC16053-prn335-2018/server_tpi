package uesocc.edu.sv.tpi2020.tpi_project.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.CollectionAttribute;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;
import uesocc.edu.sv.tpi2020.tpi_project.entities.Eventos;
import uesocc.edu.sv.tpi2020.tpi_project.entities.Usuario;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-07T12:33:56")
@StaticMetamodel(Calendario.class)
public class Calendario_ { 

    public static volatile SingularAttribute<Calendario, String> color;
    public static volatile SingularAttribute<Calendario, Usuario> idUsuario;
    public static volatile CollectionAttribute<Calendario, Eventos> eventosCollection;
    public static volatile SingularAttribute<Calendario, String> nombre;
    public static volatile SingularAttribute<Calendario, Integer> idCalendario;

}