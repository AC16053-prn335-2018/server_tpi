package uesocc.edu.sv.tpi2020.tpi_project.entities;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2020-03-07T12:33:56")
@StaticMetamodel(Registro.class)
public class Registro_ { 

    public static volatile SingularAttribute<Registro, String> navegador;
    public static volatile SingularAttribute<Registro, Integer> segundo;
    public static volatile SingularAttribute<Registro, Integer> diaSemana;
    public static volatile SingularAttribute<Registro, String> resolucion;
    public static volatile SingularAttribute<Registro, Integer> utc;
    public static volatile SingularAttribute<Registro, Integer> minuto;
    public static volatile SingularAttribute<Registro, Integer> hora;
    public static volatile SingularAttribute<Registro, String> ip;
    public static volatile SingularAttribute<Registro, Integer> mes;
    public static volatile SingularAttribute<Registro, Integer> dia;
    public static volatile SingularAttribute<Registro, Integer> anio;
    public static volatile SingularAttribute<Registro, Integer> idRegistro;

}