/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.tpi2020.tpi_project.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cristian
 */
@Entity
@Table(name = "eventos", catalog = "calendario", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Eventos.findAll", query = "SELECT e FROM Eventos e"),
    @NamedQuery(name = "Eventos.findByIdEvento", query = "SELECT e FROM Eventos e WHERE e.idEvento = :idEvento"),
    @NamedQuery(name = "Eventos.findByNombre", query = "SELECT e FROM Eventos e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Eventos.findByAnio", query = "SELECT e FROM Eventos e WHERE e.anio = :anio"),
    @NamedQuery(name = "Eventos.findByMes", query = "SELECT e FROM Eventos e WHERE e.mes = :mes"),
    @NamedQuery(name = "Eventos.findByDia", query = "SELECT e FROM Eventos e WHERE e.dia = :dia"),
    @NamedQuery(name = "Eventos.findByHora", query = "SELECT e FROM Eventos e WHERE e.hora = :hora"),
    @NamedQuery(name = "Eventos.findByMinutos", query = "SELECT e FROM Eventos e WHERE e.minutos = :minutos"),
    @NamedQuery(name = "Eventos.findBySegundos", query = "SELECT e FROM Eventos e WHERE e.segundos = :segundos"),
    @NamedQuery(name = "Eventos.findByDuracion", query = "SELECT e FROM Eventos e WHERE e.duracion = :duracion")})
public class Eventos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_evento", nullable = false)
    private Integer idEvento;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "nombre", nullable = false, length = 255)
    private String nombre;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anio", nullable = false)
    private int anio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mes", nullable = false)
    private int mes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dia", nullable = false)
    private int dia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora", nullable = false)
    private int hora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "minutos", nullable = false)
    private int minutos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segundos", nullable = false)
    private int segundos;
    @Basic(optional = false)
    @NotNull
    @Column(name = "duracion", nullable = false)
    private int duracion;
    @JoinColumn(name = "id_calendario", referencedColumnName = "id_calendario", nullable = false)
    @ManyToOne(optional = false)
    private Calendario idCalendario;

    public Eventos() {
    }

    public Eventos(Integer idEvento) {
        this.idEvento = idEvento;
    }

    public Eventos(Integer idEvento, String nombre, int anio, int mes, int dia, int hora, int minutos, int segundos, int duracion) {
        this.idEvento = idEvento;
        this.nombre = nombre;
        this.anio = anio;
        this.mes = mes;
        this.dia = dia;
        this.hora = hora;
        this.minutos = minutos;
        this.segundos = segundos;
        this.duracion = duracion;
    }

    public Integer getIdEvento() {
        return idEvento;
    }

    public void setIdEvento(Integer idEvento) {
        this.idEvento = idEvento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinutos() {
        return minutos;
    }

    public void setMinutos(int minutos) {
        this.minutos = minutos;
    }

    public int getSegundos() {
        return segundos;
    }

    public void setSegundos(int segundos) {
        this.segundos = segundos;
    }

    public int getDuracion() {
        return duracion;
    }

    public void setDuracion(int duracion) {
        this.duracion = duracion;
    }

    public Calendario getIdCalendario() {
        return idCalendario;
    }

    public void setIdCalendario(Calendario idCalendario) {
        this.idCalendario = idCalendario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idEvento != null ? idEvento.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Eventos)) {
            return false;
        }
        Eventos other = (Eventos) object;
        if ((this.idEvento == null && other.idEvento != null) || (this.idEvento != null && !this.idEvento.equals(other.idEvento))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uesocc.edu.sv.tpi2020.tpi_project.entities.Eventos[ idEvento=" + idEvento + " ]";
    }
    
}
