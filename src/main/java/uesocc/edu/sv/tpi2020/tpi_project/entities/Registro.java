/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package uesocc.edu.sv.tpi2020.tpi_project.entities;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author cristian
 */
@Entity
@Table(name = "registro", catalog = "calendario", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Registro.findAll", query = "SELECT r FROM Registro r"),
    @NamedQuery(name = "Registro.findByIdRegistro", query = "SELECT r FROM Registro r WHERE r.idRegistro = :idRegistro"),
    @NamedQuery(name = "Registro.findByUtc", query = "SELECT r FROM Registro r WHERE r.utc = :utc"),
    @NamedQuery(name = "Registro.findByAnio", query = "SELECT r FROM Registro r WHERE r.anio = :anio"),
    @NamedQuery(name = "Registro.findByMes", query = "SELECT r FROM Registro r WHERE r.mes = :mes"),
    @NamedQuery(name = "Registro.findByDia", query = "SELECT r FROM Registro r WHERE r.dia = :dia"),
    @NamedQuery(name = "Registro.findByDiaSemana", query = "SELECT r FROM Registro r WHERE r.diaSemana = :diaSemana"),
    @NamedQuery(name = "Registro.findByHora", query = "SELECT r FROM Registro r WHERE r.hora = :hora"),
    @NamedQuery(name = "Registro.findByMinuto", query = "SELECT r FROM Registro r WHERE r.minuto = :minuto"),
    @NamedQuery(name = "Registro.findBySegundo", query = "SELECT r FROM Registro r WHERE r.segundo = :segundo"),
    @NamedQuery(name = "Registro.findByIp", query = "SELECT r FROM Registro r WHERE r.ip = :ip"),
    @NamedQuery(name = "Registro.findByNavegador", query = "SELECT r FROM Registro r WHERE r.navegador = :navegador"),
    @NamedQuery(name = "Registro.findByResolucion", query = "SELECT r FROM Registro r WHERE r.resolucion = :resolucion")})
public class Registro implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id_registro", nullable = false)
    private Integer idRegistro;
    @Basic(optional = false)
    @NotNull
    @Column(name = "utc", nullable = false)
    private int utc;
    @Basic(optional = false)
    @NotNull
    @Column(name = "anio", nullable = false)
    private int anio;
    @Basic(optional = false)
    @NotNull
    @Column(name = "mes", nullable = false)
    private int mes;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dia", nullable = false)
    private int dia;
    @Basic(optional = false)
    @NotNull
    @Column(name = "dia_semana", nullable = false)
    private int diaSemana;
    @Basic(optional = false)
    @NotNull
    @Column(name = "hora", nullable = false)
    private int hora;
    @Basic(optional = false)
    @NotNull
    @Column(name = "minuto", nullable = false)
    private int minuto;
    @Basic(optional = false)
    @NotNull
    @Column(name = "segundo", nullable = false)
    private int segundo;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "ip", nullable = false, length = 255)
    private String ip;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "navegador", nullable = false, length = 255)
    private String navegador;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 255)
    @Column(name = "resolucion", nullable = false, length = 255)
    private String resolucion;

    public Registro() {
    }

    public Registro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public Registro(Integer idRegistro, int utc, int anio, int mes, int dia, int diaSemana, int hora, int minuto, int segundo, String ip, String navegador, String resolucion) {
        this.idRegistro = idRegistro;
        this.utc = utc;
        this.anio = anio;
        this.mes = mes;
        this.dia = dia;
        this.diaSemana = diaSemana;
        this.hora = hora;
        this.minuto = minuto;
        this.segundo = segundo;
        this.ip = ip;
        this.navegador = navegador;
        this.resolucion = resolucion;
    }

    public Integer getIdRegistro() {
        return idRegistro;
    }

    public void setIdRegistro(Integer idRegistro) {
        this.idRegistro = idRegistro;
    }

    public int getUtc() {
        return utc;
    }

    public void setUtc(int utc) {
        this.utc = utc;
    }

    public int getAnio() {
        return anio;
    }

    public void setAnio(int anio) {
        this.anio = anio;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getDiaSemana() {
        return diaSemana;
    }

    public void setDiaSemana(int diaSemana) {
        this.diaSemana = diaSemana;
    }

    public int getHora() {
        return hora;
    }

    public void setHora(int hora) {
        this.hora = hora;
    }

    public int getMinuto() {
        return minuto;
    }

    public void setMinuto(int minuto) {
        this.minuto = minuto;
    }

    public int getSegundo() {
        return segundo;
    }

    public void setSegundo(int segundo) {
        this.segundo = segundo;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    public String getNavegador() {
        return navegador;
    }

    public void setNavegador(String navegador) {
        this.navegador = navegador;
    }

    public String getResolucion() {
        return resolucion;
    }

    public void setResolucion(String resolucion) {
        this.resolucion = resolucion;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (idRegistro != null ? idRegistro.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Registro)) {
            return false;
        }
        Registro other = (Registro) object;
        if ((this.idRegistro == null && other.idRegistro != null) || (this.idRegistro != null && !this.idRegistro.equals(other.idRegistro))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "uesocc.edu.sv.tpi2020.tpi_project.entities.Registro[ idRegistro=" + idRegistro + " ]";
    }
    
}
